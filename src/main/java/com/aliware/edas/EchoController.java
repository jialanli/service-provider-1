package com.aliware.edas;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wanglan
 */
@RestController
public class EchoController {
	
	@Value("${server.port}")
	private int port ;
	
    @RequestMapping(value = "/echo/{string}", method = RequestMethod.GET)
    public String echo(@PathVariable String string) {
        return "服务service-provider"+port+"正在被调用，/echo/{string}返回："+string;
    }
}